import sys

class Tree:
    def __init__(self,name : str, parent = None, data : bytes = None):
        print('\n')
        self.parent = parent
        self.name = name
        self.pointer = 0
        self.data = data
        self.magicToSection = {
            b'ftyp':self.undefined,#parse_ftyp, # root
            b'moov':self.nestedContainers,#parse_moov, # root
            b'udta':self.udta, # moov
            b'mdat':self.nestedContainers, # root
            b'wide':self.undefined,
            b'free':self.undefined, # root
            b'avc5':self.undefined,
            b'mvhd':self.undefined, # moov
            b'trak':self.nestedContainers,#parse_trak, # moov
            b'tkhd':self.undefined, # trak
            b'edts':self.undefined, # trak
            b'mdia':self.nestedContainers,#parse_mdia, # trak
            b'hdlr':self.undefined, # mdia
            b'minf':self.undefined, # mdia
            b'iods':self.undefined,
            b'elst':self.undefined,
            b'mdhd':self.undefined, # mdia
        }
        self.containers = {

        }
        return
    # TOOLS
    def start(self, data : bytes,offset : int, magic : bytes = None):
        print('Magic: %s' % magic)
        print('Container Size: %s' % (offset+8))
        print('Data Size: %s' % len(data))
        print(data[0:8])
        return
    
    def advancePointer(self, count : int) -> bytes:
        self.pointer+=count

    def getNextBytes(self,count : int):
        return self.data[self.pointer:self.pointer+count]

    def parse(self, end : int):
        while True:
            print('%s Pointer:  %s' % (self.name,self.pointer))
            offset = int.from_bytes(self.getNextBytes(4),'big')
            magic = self.getNextBytes(8)[4:]
            if(magic == b''):
                print('end of %s' % self.name)
                break

            if(not self.magicToSection.__contains__(magic)):
                print(magic)
                print('Unknown Magic')
                file = open('unknown.log','a+')
                file.write(str(magic))
                file.write('\n')
                file.close()
                input("Press Enter to Continue")
                break
            self.magicToSection.get(magic)(self.data[self.pointer:self.pointer+offset],offset,magic) # Minus 8 for magic and offset
            self.advancePointer(offset)
            print('\n')
        return self.containers

    # HANDLERS

    def undefined(self, data: bytes, offset : int, magic : bytes = None):
        self.start(data,offset,magic)
        self.containers[magic] = Container(None, data, self.pointer, self.pointer+offset)
        return
    
    def udta(self, data: bytes, offset : int, magic : bytes = None):
        self.undefined(data,offset,magic)
        print(data)
        exit()
    
    def nestedContainers(self, data: bytes, offset : int, magic : bytes = None):
        self.start(data,offset,magic)
        container = Container(None, data, self.pointer, self.pointer+offset)
        tree = Tree(magic,self,data)
        tree.pointer = 8
        containers = tree.parse(len(data))
        self.containers[magic] = container
        for k,v in containers.items():
            v.parent = container
            self.containers[k] = v

    def endOfFile(self, data : bytes, offset : int, magic : bytes = None):
        exit()
        return

class Container:
    def __init__(self, parent , data : bytes,start : int, end : int):
        self.parent = parent
        self.data = data
        self.start = start
        self.end = end
        pass
    def getMagic(self) -> bytes:
        return self.data[4:8]

    def getOffset(self) -> int:
        return int.from_bytes(self.data[:4],'big')

    def setOffset(self,value : int):
        newSize = value.to_bytes(5,'big')
        oldSize = self.getOffset()
        delta = oldSize-value
        self.data = newSize + self.data[4:len(self.data)-delta]
        if self.parent != None:
            self.parent.setOffset(self.parent.getOffset()-delta)

    def getData(self) -> bytes:
        return self.data

    def setData(self,data : bytes):
        pass

    

# file = open(sys.argv[1].replace(r'.\\',''),'rb+')
# file = open('Told by Shift on Vimeo-1.mp4','rb+')

def default(file):
    _data : bytes = file.read()
    maxLength : int = len(_data)
    tree = Tree('Root',None,_data)
    containers = tree.parse(maxLength)
    for v in containers.values():
        print(v.getMagic())
        print(v.getOffset())
    containers[b'udta'].setOffset(8)
    print('done')

def forDir():
    import os
    for x in os.listdir():
        print(x)
        if not x.endswith('.mp4'):
            continue
        file = open(x,'rb+')
        default(file)
        file.close()

# default(open(sys.argv[1].replace(r'.\\',''),'rb+'))
default(open('HTPwzRP.mp4','rb+'))
# # forDir()