import sys

class Tree:
    def __init__(self,name : str, startPos : int, parent = None):
        print('\n')
        self.parent = parent
        self.startPos = startPos
        self.name = name
        self.pointer = 0
        self.magicToSection = {
            b'ftyp':self.parse_header_ftyp,
            b'moov':self.parse_media_header_moov,
            b'udta':self.parse_udta,
            b'mdat':self.parse_metadata_mdat,
            b'wide':self.undefined,
            b'free':self.undefined,
            b'avc5':self.undefined,
            b'mvhd':self.undefined,
            b'trak':self.parse_trak,
            b'tkhd':self.undefined,
            b'edts':self.undefined,
            b'mdia':self.undefined,
        }
        return
    
    def start(self,data : bytes,offset : int):
        print('Offset: %s' % offset)
        print('Size: %s' % len(data))
        return

    def parse(self, data : bytes, end : int):
        while True:
            print('%s Pointer:  %s' % (self.name,self.pointer))
            offset = int.from_bytes(self.next(data,4),'big')
            magic = self.next(data,4)
            if(magic == b''):
                print('end of %s' % self.name)
                break
            print('Magic: %s' % magic)

            if(not self.magicToSection.__contains__(magic)):
                print('Unknown Magic')
                print('Offset : %s' % self.getNonRelativePos(4))
                print('Offset : %s' % hex(self.getNonRelativePos(4)))
                break
            self.magicToSection.get(magic)(data,offset-8) # Minus 8 for magic and offset
            print('\n')
        return

    def getNonRelativePos(self,offset : int) -> int:
        tree = self.parent
        pos = self.pointer-offset
        while tree != None:
            pos+=tree.startPos
            tree = tree.parent
        return pos+self.startPos

    def parse_udta(self, data : bytes, offset : int):
        self.start(data,offset)
        tree = self.parent
        pos = 0
        while tree != None:
            pos+=tree.pointer
            tree = tree.parent
        pos-=offset
        pos-=8
        print('UDTA RANGE: %s:%s' % (pos,pos+offset+8 + 1)) # +1 for exclusive
        self.next(data, offset)


    def parse_trak(self, data : bytes, offset : int):
        self.start(data,offset)
        trak_data = self.next(data,offset)
        tree = Tree('trak', self.pointer-offset, self)
        tree.parse(trak_data,len(trak_data))

    def undefined(self, data: bytes,offset : int):
        self.start(data,offset)
        self.next(data,offset)
        return

    def next(self,data : bytes, count : int) -> bytes:
        self.pointer+=count
        return data[self.pointer-count:self.pointer]

    def parse_header_ftyp(self, data : bytes,id_offset : int):
        # Header
        self.start(data,id_offset)
        # Get Brand
        id_section = self.next(data,id_offset) # 8 for the offset and magic
        major_brand = id_section[0:4]
        major_brand_version = id_section[4:8]
        print("Major brand: \n%s:%s" % (major_brand,int.from_bytes(major_brand_version,'big')))
        # Supported formats
        formats = []
        t_point = 8
        while t_point < id_offset:
            formats.append(id_section[t_point:t_point+4])
            t_point+=4
        print("Compatible formats: \n%s" % formats)
        return

    def parse_media_header_moov(self, data: bytes,offset : int):
        self.start(data,offset)
        moov_data = self.next(data,offset)
        tree = Tree('moov', self.pointer-offset,self)
        tree.parse(moov_data,len(moov_data))
        return

    def parse_metadata_mdat(self, data : bytes,offset : int):
        self.start(data,offset)
        mdat_data = self.next(data,offset)
        tree = Tree('mdat', self.pointer-offset,self)
        tree.parse(mdat_data,len(mdat_data))

    def endOfFile(self, data : bytes,offset : int):
        exit()
        return

# file = open(sys.argv[1].replace(r'.\\',''),'rb+')
file = open('HTPwzRP.mp4','rb+')

_data : bytes = file.read()
maxLength : int = len(_data)
tree = Tree('Root',0,None)
tree.parse(_data,maxLength)